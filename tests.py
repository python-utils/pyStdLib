from unittest import TestCase
import matplotlib.pyplot as plt
from pyStdLib.misc.signal_gen import signal_gen
import pyStdLib
# from pyStdLib.misc.signal_gen import signal_gen

plt.ion()

class HelloworldTestCase(TestCase):
    def test_helloworld(self):
        x = signal_gen()
        y = signal_gen()/2

        pyStdLib.plot_lib.plot_lib.plot_reim((x, y))
        pyStdLib.plot_lib.plot_lib.plot_psd((x, y))
        input('Press any key to continue...')
        return 0