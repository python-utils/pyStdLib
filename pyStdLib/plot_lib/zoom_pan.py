class ZoomPan:
    def __init__(self):
        self.press = None
        self.cur_xlim = None
        self.cur_ylim = None
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.xpress = None
        self.ypress = None
        self.active_ax = 'xy'

    def zoom_factory(self, ax, base_scale = 2.):
        def zoom(event):
            cur_xlim = ax.get_xlim()
            cur_ylim = ax.get_ylim()

            # print(event)

            if event.inaxes == ax:

                if event.key == 'shift':
                    active_ax = 'x'
                elif event.key == 'control':
                    active_ax = 'y'
                else:
                    active_ax = 'xy'

                xdata = event.xdata # get event x location
                ydata = event.ydata # get event y location

                if event.button == 'down':
                    # deal with zoom in
                    scale_factor = 1 / base_scale
                elif event.button == 'up':
                    # deal with zoom out
                    scale_factor = base_scale
                else:
                    # deal with something that should never happen
                    scale_factor = 1
                    print(event.button)

                new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
                new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

                relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
                rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

                if active_ax == 'x' or active_ax == 'xy':
                    ax.set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
                if active_ax == 'y' or active_ax == 'xy':
                    ax.set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
                ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest
        fig.canvas.mpl_connect('scroll_event', zoom)

        return zoom

    def pan_factory(self, ax):
        def onPress(event):
            if event.inaxes == ax:

                if event.dblclick:
                    print('doubleclick')
                    ax.autoscale(enable=True)
                else:
                    if event.key == 'shift':
                        self.active_ax = 'y'
                    elif event.key == 'control':
                        self.active_ax = 'x'
                    else:
                        self.active_ax = 'xy'

                    self.cur_xlim = ax.get_xlim()
                    self.cur_ylim = ax.get_ylim()
                    self.press = self.x0, self.y0, event.xdata, event.ydata
                    self.x0, self.y0, self.xpress, self.ypress = self.press

        def onRelease(event):
            self.press = None
            ax.figure.canvas.draw()

        def onMotion(event):
            if self.press is None: return
            if event.inaxes != ax: return

            if self.active_ax == 'x' or self.active_ax == 'xy':
                dx = event.xdata - self.xpress
                self.cur_xlim -= dx
                ax.set_xlim(self.cur_xlim)

            if self.active_ax == 'y' or self.active_ax == 'xy':
                dy = event.ydata - self.ypress
                self.cur_ylim -= dy
                ax.set_ylim(self.cur_ylim)

            ax.figure.canvas.draw()

        # def onBtnPress(event):
        #     # print('you pressed', event.key, event.xdata, event.ydata)
        #     if event.key == ' ':
        #         # print('SPACE')
        #         self.cur_xlim = ax.get_xlim()
        #         self.cur_ylim = ax.get_ylim()
        #         self.press = self.x0, self.y0, event.xdata, event.ydata
        #         self.x0, self.y0, self.xpress, self.ypress = self.press
        #
        # def onBtnRelease(event):
        #     if event.key == ' ':
        #         self.press = None
        #         ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest

        # attach the call back
        fig.canvas.mpl_connect('button_press_event',onPress)
        fig.canvas.mpl_connect('button_release_event',onRelease)
        fig.canvas.mpl_connect('motion_notify_event',onMotion)
        # fig.canvas.mpl_connect('key_press_event',onBtnPress)
        # fig.canvas.mpl_connect('key_release_event',onBtnRelease)

        #return the function
        return onMotion


if __name__ == '__main__':
    import numpy
    from matplotlib.pyplot import figure, show
    import matplotlib.pyplot as plt
    fig = figure()

    # ax = fig.add_subplot(111, xlim=(0, 1), ylim=(0, 1), autoscale_on=False)

    # ax.set_title('Click to zoom')
    x, y, s, c = numpy.random.rand(4, 200)
    s *= 200

    # ax.scatter(x, y, s, c)
    plt.figure()
    plt.subplot(2,1,1)
    plt.scatter(x, y, s, c)

    ax = plt.gca()
    scale = 1.1
    zp = ZoomPan()
    figZoom = zp.zoom_factory(ax, base_scale=scale)
    figPan = zp.pan_factory(ax)

    plt.subplot(2,1,2)
    plt.scatter(x, y, s, c)

    ax = plt.gca()
    scale = 1.1
    zp = ZoomPan()
    figZoom = zp.zoom_factory(ax, base_scale=scale)
    figPan = zp.pan_factory(ax)

    show()
