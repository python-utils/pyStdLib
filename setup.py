from setuptools import setup, find_packages
from os.path import join, dirname

import pyStdLib

setup(
    name='pyStdLib',
    version=pyStdLib.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    install_requires=[
    'matplotlib',
    'numpy',
    'scipy',
    ],
    test_suite='tests',
    url='https://gitlab.com/python-utils/pyStdLib.git'
)